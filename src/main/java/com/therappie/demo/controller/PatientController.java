package com.therappie.demo.controller;

import com.therappie.demo.model.Patient;
import com.therappie.demo.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@RestController
public class PatientController {

    private PatientService service;

    @Autowired
    public PatientController(PatientService service) {
        this.service = Objects.requireNonNull(service, "Patient Service must be defined.");
    }

    @GetMapping("/patient-form")
    public ModelAndView registerForm() {
        return new ModelAndView("patient/patient-form")
                .addObject("patient", new Patient());
    }

    @PostMapping("/register-patient")
    public ModelAndView register(@ModelAttribute Patient patient) {
        service.save(patient);
        return new ModelAndView("patient/patients-list")
                .addObject("patients", service.getAll());
    }

    @PostMapping("/edit-patient-profile")
    public ModelAndView edit(@ModelAttribute Patient patient) {

        service.save(patient);
        return new ModelAndView("patient/patients-list")
                .addObject("patients", service.getAll());
    }

    @GetMapping("/patients-list")
    public ModelAndView patientList() {
        return new ModelAndView("patient/patients-list")
                .addObject("patients", service.getAll());
    }

    @GetMapping("/patient-profile/")
    public ModelAndView patientProfile(@RequestParam String firstName, @RequestParam String lastName) {
        return new ModelAndView("patient/patient-profile")
                .addObject("patient", service.getByName(firstName, lastName));
    }

    @GetMapping("/patient-profile/{id}")
    public ModelAndView patientProfileById(@PathVariable Long id) {
        return new ModelAndView("patient/patient-profile")
                .addObject("patient", service.getById(id).get());
    }

    @GetMapping("/edit-patient-profile/{id}")
    public ModelAndView editPatientProfileById(@PathVariable Long id) {
        return new ModelAndView("patient/patient-profile-edit")
                .addObject("patient", service.getById(id).get());
    }

//    @PostMapping
//    public ModelAndView save (@RequestBody Patient patient) {
//        service.save(patient);
//        return ModelAndView.     status(HttpStatus.CREATED).build();
//    }
}
