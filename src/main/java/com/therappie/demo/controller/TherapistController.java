package com.therappie.demo.controller;

import com.therappie.demo.model.Therapist;
import com.therappie.demo.service.TherapistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@RestController
public class TherapistController {

    private TherapistService service;

    @Autowired
    public TherapistController(TherapistService service){
        this.service = Objects.requireNonNull(service, "Therapist Service must be defined.");
    }


    @GetMapping("/therapist-form")
    public ModelAndView registerForm() {
        return new ModelAndView("therapist/therapist-form")
                .addObject("therapist", new Therapist());
    }

    @PostMapping("/register-therapist")
    public ModelAndView register(@ModelAttribute Therapist therapist){
        service.save(therapist);
        return new ModelAndView("index");
    }
}