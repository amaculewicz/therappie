package com.therappie.demo.controller;

import com.therappie.demo.model.Therapy;
import com.therappie.demo.service.PatientService;
import com.therappie.demo.service.SessionService;
import com.therappie.demo.service.TherapistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@RestController
public class TherapyController {

    private SessionService service;
    private PatientService patientService;
    private TherapistService therapistService;


    @Autowired
    public TherapyController(SessionService service, PatientService patientService, TherapistService therapistService) {
        this.service = Objects.requireNonNull(service, "Therapy Service must be defined.");
        this.patientService = Objects.requireNonNull(patientService, "Patient Service must be defined.");
        this.therapistService = Objects.requireNonNull(therapistService, "Therapist Service must be defined.");
    }

    @GetMapping("/therapy-form")
    public ModelAndView registerForm() {
        return new ModelAndView("therapy/therapy-form")
                .addObject("therapy", new Therapy())
                .addObject("patients", patientService.getAll())
                .addObject("therapists", therapistService.getAll());
    }


    @PostMapping("/register-therapy")
    public ModelAndView registerSession(@ModelAttribute Therapy therapy) {
        service.save(therapy);
        return new ModelAndView("therapy/therapy-list")
                .addObject("therapies", service.getAll());
    }

    @GetMapping("/therapy-list")
    public ModelAndView therapyList() {
        return new ModelAndView("therapy/therapy-list")
                .addObject("therapies", service.getAll());
    }

    @GetMapping("/therapy-profile/")
    public ModelAndView sessionProfile(@RequestParam Long id) {
        return new ModelAndView("patient/patient-profile")
                .addObject("patient", service.getById(id));
    }

    @GetMapping("/edit-therapy-profile/{id}")
    public ModelAndView editSessionProfileById(@PathVariable Long id) {
        return new ModelAndView("therapy/therapy-profile-edit")
                .addObject("therapy", service.getById(id).get());
    }
}
