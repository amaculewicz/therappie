package com.therappie.demo.service;

import com.therappie.demo.model.Patient;
import com.therappie.demo.repositories.PatientRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class PatientService {

    private PatientRepository repository;

    public PatientService(PatientRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Patient Repository must be defined");
    }

    public void save(Patient patient) {
        repository.save(patient);
    }

    public List<Patient>getAll() {
        return repository.findAll();
    }

    public Optional<Patient> getById(Long id) {
        return repository.findById(id);
    }

    public Patient getByName(String firstName, String lastName) {
        return repository.findByFirstNameAndLastName(firstName, lastName);
    }

}
