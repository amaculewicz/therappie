package com.therappie.demo.service;

import com.therappie.demo.model.Therapy;
import com.therappie.demo.repositories.TherapyRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class SessionService {

    private TherapyRepository repository;

    public SessionService(TherapyRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Therapy Repository must be defined");
    }

    public void save(Therapy therapy) {
        repository.save(therapy);
    }

    public List<Therapy>getAll() {
        return repository.findAll();
    }

    public Optional<Therapy> getById(Long id) {
        return repository.findById(id);
    }

//    public Therapy getByName(LocalDate date) {
//        return repository.findByDate(date);
//    }

}
