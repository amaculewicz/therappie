package com.therappie.demo.service;

import com.therappie.demo.model.Patient;
import com.therappie.demo.model.Therapist;
import com.therappie.demo.repositories.PatientRepository;
import com.therappie.demo.repositories.TherapistRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class TherapistService {

    private TherapistRepository repository;

    public TherapistService(TherapistRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Therapist Repository must be defined");
    }

    public void save(Therapist therapist) {
        repository.save(therapist);
    }

    public List<Therapist>getAll() {
        return repository.findAll();
    }

    public Optional<Therapist> getById(Long id) {
        return repository.findById(id);
    }

    public Therapist getByName(String firstName, String lastName) {
        return repository.findByFirstNameAndLastName(firstName, lastName);
    }
}
