package com.therappie.demo.repositories;

import com.therappie.demo.model.Patient;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PatientRepository extends CrudRepository<Patient, Long> {
     List<Patient> findAll();

    Patient findByFirstNameAndLastName(String firstName, String lastName);
}
