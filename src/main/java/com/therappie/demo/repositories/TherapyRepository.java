package com.therappie.demo.repositories;
import com.therappie.demo.model.Therapy;
import org.springframework.data.repository.CrudRepository;


import java.util.List;
import java.util.Optional;

public interface TherapyRepository extends CrudRepository<Therapy, Long> {
     List<Therapy> findAll();

    Optional<Therapy> findById(Long id);
}
