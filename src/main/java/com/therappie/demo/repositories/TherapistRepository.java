package com.therappie.demo.repositories;

import com.therappie.demo.model.Patient;
import com.therappie.demo.model.Therapist;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TherapistRepository extends CrudRepository<Therapist, Long> {
     List<Therapist> findAll();
//TODO
    Therapist findByFirstNameAndLastName(String firstName, String lastName);
}
